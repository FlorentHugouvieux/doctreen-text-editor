import RfcEditor from "./rfc-ckeditor";
import ReportEditor from "./report-ckeditor";

export default {
  RfcEditor,
  ReportEditor,
};
